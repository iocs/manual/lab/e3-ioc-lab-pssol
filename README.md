# e3-ioc-lab-pssol

IOC for tests with the Solenoid PS model TDK Lambda

---

## IOC template

This project was generated with the [E3 cookiecutter IOC template](https://gitlab.esss.lu.se/ics-cookiecutter/cookiecutter-e3-ioc).
