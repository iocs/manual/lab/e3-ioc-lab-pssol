require essioc
require tdklambdagenesys

iocshLoad("$(essioc_DIR)/common_config.iocsh")

# MOXA
epicsEnvSet("IP_ADDR","ics-lab-moxa.cslab.esss.lu.se")
epicsEnvSet("IP_PORT","4001")

# Prefixes
epicsEnvSet("P","LabS-RFLab:")
epicsEnvSet("R","RFS-PSSol-001:")

# Model
epicsEnvSet("TDK_MODEL","GEN300-17")
epicsEnvSet("TDK_INTERFACE","SERIAL")
epicsEnvSet("TDK_LANMODULE","0")

iocshLoad("$(tdklambdagenesys_DIR)tdklambdagenesys.iocsh", "MODEL=$(TDK_MODEL), DEVICETYPE=$(TDK_INTERFACE), LANMODULE=$(TDK_LANMODULE), CONNECTIONNAME=PSSol1, IP_ADDR=$(IP_ADDR), IP_PORT=$(IP_PORT), P=$(P), R=$(R)")
